package com.example.library.github.promeg.pinyinhelper.jmh;


import com.example.library.github.promeg.pinyinhelper.PinyinMapDict;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public final class FullDiffDict extends PinyinMapDict {
    static volatile FullDiffDict singleton = null;
    final Map<String, String[]> mDict = new HashMap();

    private FullDiffDict() {
        this.init();
    }

    public static FullDiffDict getInstance() {
        if (singleton == null) {
            Class var0 = FullDiffDict.class;
            synchronized(FullDiffDict.class) {
                if (singleton == null) {
                    singleton = new FullDiffDict();
                }
            }
        }

        return singleton;
    }

    public Map<String, String[]> mapping() {
        return this.mDict;
    }

    private void init() {
        BufferedReader reader = null;

        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("jmhdic/pinyin_diff.dic");
            reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while((line = reader.readLine()) != null) {
                String[] keyAndValue = line.split("\\s+");
                if (keyAndValue != null && keyAndValue.length == 2) {
                    String[] pinyinStrs = keyAndValue[0].split("'");
                    this.mDict.put(keyAndValue[1], pinyinStrs);
                }
            }
        } catch (IOException var15) {
            var15.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException var14) {
                    var14.printStackTrace();
                }
            }

        }

    }
}
