package com.example.library.github.promeg.pinyinhelper.trie;


import com.example.library.github.promeg.pinyinhelper.interval.Interval;
import com.example.library.github.promeg.pinyinhelper.interval.Intervalable;

public class Emit extends Interval implements Intervalable {
    private final String keyword;

    public Emit(int start, int end, String keyword) {
        super(start, end);
        this.keyword = keyword;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public String toString() {
        return super.toString() + "=" + this.keyword;
    }
}
