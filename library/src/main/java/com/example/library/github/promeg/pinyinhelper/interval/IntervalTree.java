package com.example.library.github.promeg.pinyinhelper.interval;


import com.example.library.github.promeg.pinyinhelper.trie.Emit;

import java.util.*;

public class IntervalTree {
    private IntervalNode rootNode = null;

    public IntervalTree(List<Emit> intervals) {
        this.rootNode = new IntervalNode(intervals);
    }

    public List<Emit> removeOverlaps(List<Emit> intervals) {
        Collections.sort(intervals, new IntervalableComparatorBySize());
        Set<Intervalable> removeIntervals = new TreeSet();
        Iterator var3 = intervals.iterator();

        Intervalable removeInterval;
        while(var3.hasNext()) {
            removeInterval = (Intervalable)var3.next();
            if (!removeIntervals.contains(removeInterval)) {
                removeIntervals.addAll(this.findOverlaps(removeInterval));
            }
        }

        var3 = removeIntervals.iterator();

        while(var3.hasNext()) {
            removeInterval = (Intervalable)var3.next();
            intervals.remove(removeInterval);
        }

        Collections.sort(intervals, new IntervalableComparatorByPosition());
        return intervals;
    }

    public List<Intervalable> findOverlaps(Intervalable interval) {
        return this.rootNode.findOverlaps(interval);
    }
}
