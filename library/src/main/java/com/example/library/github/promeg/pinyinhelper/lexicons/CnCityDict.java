package com.example.library.github.promeg.pinyinhelper.lexicons;

import com.example.library.github.promeg.pinyinhelper.PinyinMapDict;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public final class CnCityDict extends PinyinMapDict {
    static volatile CnCityDict singleton = null;
    final Map<String, String[]> mDict = new HashMap();

    private CnCityDict() {
        this.init();
    }

    public static CnCityDict getInstance() {
        if (singleton == null) {
            Class var0 = CnCityDict.class;
            synchronized(CnCityDict.class) {
                if (singleton == null) {
                    singleton = new CnCityDict();
                }
            }
        }

        return singleton;
    }

    public Map<String, String[]> mapping() {
        return this.mDict;
    }

    private void init() {
        BufferedReader reader = null;

        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("tinypinyin/cncity.txt");
            reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while((line = reader.readLine()) != null) {
                String[] keyAndValue = line.split("\\s+");
                if (keyAndValue != null && keyAndValue.length == 2) {
                    String[] pinyinStrs = keyAndValue[0].split("'");
                    this.mDict.put(keyAndValue[1], pinyinStrs);
                }
            }
        } catch (IOException var15) {
            var15.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException var14) {
                    var14.printStackTrace();
                }
            }

        }

    }
}
